/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.io.*;
import java.util.*;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor {
	//Cambio a private
	private static Scanner a;
	
	public static void main(String[] args) { //LLave
	
		a = new Scanner(System.in);
		//Separación de variables
		int n;
		int cantidad_maxima_alumnos = 10;
		int arrays[] = new int[10];
		//Espacios
		for (n = 0; n < 10; n++){
			System.out.println("Introduce nota media de alumno");
			arrays[n] = a.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));
		
		a.close();
	}
	//Cambio a public
	public static double recorrer_array(int vector[]){
		//Espacios y llave
		double c = 0;
		for(int a = 0;a < 10; a++) {
			c = c + vector[a];
		}
		return c/10;
	}
	
}