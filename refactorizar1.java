/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.*;
import java.util.*;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class refac {
	
	final static String cad = "Bienvenido al programa";
	
	public static void main(String[] args) {//LLave
		//Separación de variables
		String cadena1;
		String cadena2;
		Scanner c = new Scanner(System.in);
		
		System.out.println(cad);
		System.out.println("Introduce tu dni");
		cadena1 = c.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = c.nextLine();
		//Separación de variables
		int a=7; 
		int b=16;
		int numeroc = 25;
		//Espacios y llaves
		if(a > b || numeroc % 5 != 0 && (numeroc*3-1) > (b / numeroc)){
			System.out.println("Se cumple la condición");
		}
		//Espacios
		numeroc = a + (b * numeroc) + (b / a);
		
		String array[] = new String[7];
		String[] array = { "Lunes", "Martes", "Miercoles", "jueves", "viernes", "sabado", "domingo" };  

		
		recorrer_array(array);
	}
	static void recorrer_array(String vectordestrings[]){ // Llaves y espacios
		for(int dia = 0; dia < 7; dia++) {
			System.out.println("El dia de la semana en el que te encuentras [" + (dia + 1) + "-7] es el dia: " + vectordestrings[dia]);
		}
	}
	
}
